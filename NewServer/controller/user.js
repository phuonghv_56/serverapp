//var connection = require('../connection');
var mysql = require('mysql');
var client;
var escapeSQL = require('sqlstring');
function startConnection() {
    console.error('CONNECTING');
    client = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: 'makeyourlove',
        database: 'Common'

    });
    client.connect(function(err) {
        if (err) {
            console.error('CONNECT FAILED CONVERSATION', err.code);
            startConnection();
        } else {
            console.error('CONNECTED CONVERSATION');
        }
    });
    client.on('error', function(err) {
        if (err.fatal)
            startConnection();
    });
}
startConnection();

client.on('error', function(err) {
    if (err.fatal) {
        startConnection();
    }
});

client.query("SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci", function(error, results, fields) {
    if (error) {
        console.log(error);
    } else {
        console.log("SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci");
    }
});

client.query("SET CHARACTER SET utf8mb4", function(error, results, fields) {
    if (error) {
        console.log(error);
    } else {
        console.log("SET CHARACTER SET utf8mb4");
    }
});


function Todo() {

    this.get = function(res) {

        client.query('select *from Users', function(err, result) {

            //con.release();
            console.log(result);
            res.send(result);

        });

    };

    this.create = function(field_data, res) {

        
        var query = escapeSQL.format("SELECT * FROM `Users` WHERE `key_fir`='" + field_data.key_fir + "'");
        
        console.log(field_data);

        client.query(query, function(err, data, f) {
            if (err) {
                console.log("");
                res.send({ error: 1, message: "Server error please try again" });
            } else {
                
                console.log(data.length);

                if (data.length > 0) {
                    console.log("USer "+field_data.key_fir + " is exit");
                    res.send({ error: 1, message: "Server error please try again" });
                }else{
                    var queryInsert = escapeSQL.format("INSERT INTO `Users` SET ? ",field_data);
                    client.query(queryInsert,function(er,result){
                        if (er){
                            console.log("Add user error" + er);
                            res.send({ error: 1, message: "Add user error" });
                        }else{
                            console.log("Add user success");
                            res.send({ error: 0, message: "Add user success" });
                        }
                    });
                }
            }
        });

    };

    this.update = function(field_data, res) {

        client.query('update Users set ? where `key_fir` = ?', [field_data, field_data.key_fir], function(err, result) {

            // con.release();
            if (err) {
                console.log(err);
                res.send({ status: 1, message: "Update data to users failed" });
            } else {
                console.log(err);
                res.send({ status: 0, message: "Update data to users success" });
            }

        });
    };

    this.read = function(req, res) {

        //  console.log(id);
        client.query('select * from Users where key = ?', [req.params.id], function(err, result,f) {

            console.log(result);
            res.send(result);
        });

    };

    this.delete = function(id, res) {

        client.query('delete from Users where key = ?', [id], function(err, result) {

            //con.release();
            if (err) {
                console.log(result);
                res.send({ status: 1, message: "Delete failed" });
            } else {
                console.log(result);
                res.send({ status: 0, message: "Delete success" });
            }

        });
    };

};

module.exports = new Todo();