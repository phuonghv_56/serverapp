//var connection = require('../connection');
var mysql = require('mysql');
var escapeSQL = require('sqlstring');
var client;

function startConnection() {
    console.error('CONNECTING');
    client = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: 'makeyourlove',
        database: 'Common'

    });
    client.connect(function(err) {
        if (err) {
            console.error('CONNECT FAILED CONVERSATION', err.code);
            startConnection();
        } else {
            console.error('CONNECTED CONVERSATION');
        }
    });
    client.on('error', function(err) {
        if (err.fatal)
            startConnection();
    });
}
startConnection();


client.on('error', function(err) {
    if (err.fatal) {
        startConnection();
    }
});

client.query("SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci", function(error, results, fields) {
    if (error) {
        console.log(error);
    } else {
        console.log("SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci");
    }
});

client.query("SET CHARACTER SET utf8mb4", function(error, results, fields) {
    if (error) {
        console.log(error);
    } else {
        console.log("SET CHARACTER SET utf8mb4");
    }
});

function Notification() {

    this.get = function(res) {

        client.query('select *from Users', function(err, result) {

            //con.release();
            console.log(result);
            res.send(result);

        });

    };

    this.create = function(field_data, res) {

        var queryInsert = "INSERT INTO `Notification` SET ? "
        client.query(queryInsert, field_data, function(er, result, f) {
            if (er) {
                res.send({ error: 1, message: "Insert notification error" });
            } else {
                res.send({ error: 0, message: "Insert notification success" });
            }
        });

    };

    this.update = function(field_data, res) {

        client.query('update Users set ? where id = ?', [field_data, field_data.id], function(err, result) {

            // con.release();
            if (err) {
                console.log(result);
                res.send({ status: 1, message: "Update data to users failed" });
            } else {
                console.log(result);
                res.send({ status: 0, message: "Update data to users success" });
            }

        });
    };

    this.read = function(req, res) {

        //  console.log(id);
        client.query('select * from Users where key = ?', [req.params.id], function(err, result) {

            console.log(result);
            res.send(result);
        });

    };

    this.delete = function(id, res) {

        client.query('delete from Users where key = ?', [id], function(err, result) {

            //con.release();
            if (err) {
                console.log(result);
                res.send({ status: 1, message: "Delete failed" });
            } else {
                console.log(result);
                res.send({ status: 0, message: "Delete success" });
            }

        });
    };

};

module.exports = new Notification();